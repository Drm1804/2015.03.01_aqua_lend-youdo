//Готовность страницы
$(document).ready(function () {
    $(".fancybox").fancybox();

    $('.map_view').click(function(){
        var th = $(this);
        ymaps.ready(init);
        var myMap;

        $('.map_view').fancybox({padding:0,height:400,afterShow : function() {
            var center_x = parseFloat(th.parent().find('.coord-x').text());
            var center_y = parseFloat(th.parent().find('.coord-y').text());

            myMap = new ymaps.Map('popup-map', {
                center: [center_x,center_y],
                zoom: 15
            });

            var myPlacemark = new ymaps.Placemark([center_x,center_y], {});

            myMap.geoObjects.add(myPlacemark);

        }, afterClose:function (){
            $('#popup-map').html('');
        }});
    });

    $('.priem').click(function(){
        var title = $(this).parent().parent().find('.title').text();
        var form_name = $(this).parent().find('.form-name').text();
        $('.object').text(title);
        $('#fan input[name="location"]').val(form_name);
    });
    $('.viv').click(function(){
        var title = $(this).parent().find('.title').text();
        var form_name = $(this).parent().find('.form-name').text();
        $('.object').text(title);
        $('#fan input[name="location"]').val(form_name);
    });

    $("#top-form").validate({
        rules:{
            name:{
                required: true
            },
            phone:{
                required: true
            }
        },

        messages:{
            name:{
                required: "Введите имя"
            },
            phone:{
                required: "Введите телефон"
            }
        }
    });

    $("#action-form").validate({
        rules:{
            name:{
                required: true
            },
            phone:{
                required: true
            }
        },

        messages:{
            name:{
                required: "Введите имя"
            },
            phone:{
                required: "Введите телефон"
            }
        }
    });

    $("#bottom-form").validate({
        rules:{
            question:{
                required: true
            },
            phone:{
                required: true
            }
        },

        messages:{
            question:{
                required: "Введите вопрос"
            },
            phone:{
                required: "Введите телефон"
            }
        }
    });

    $("#popup-form").validate({
        rules:{
            name:{
                required: true
            },
            phone:{
                required: true
            }
        },

        messages:{
            name:{
                required: "Введите имя"
            },
            phone:{
                required: "Введите телефон"
            }
        }
    });

    $("#callback-form").validate({
        rules:{
            name:{
                required: true
            },
            phone:{
                required: true
            }
        },

        messages:{
            name:{
                required: "Введите имя"
            },
            phone:{
                required: "Введите телефон"
            }
        }
    });

})