/*------------Таймер------------------------*/
/*var limit = 519342; // в секундах

function processTimer(){

  if (limit > 0) {
    setTimeout("processTimer()",1000);
    limit--;
  } else {
  alert("Время вышло");
  }
  var limit_str = "";
  var deys = parseInt((limit / 3600) / 24);
  var hours = parseInt((limit-(deys*3600*24))/3600);
  var minutes = parseInt( ((limit-(deys*3600*24))- (hours*3600))/60);
  var seconds = parseInt(limit-(deys*3600*24+hours*3600+minutes*60));
  if (deys < 10) deys = "0" + deys;
  if (hours < 10) hours = "0" + hours;
  if (minutes < 10) minutes = "0" + minutes;
  if (seconds < 10) seconds = "0" + seconds;
  limit_str = "<div class='timer_block'><div class='deys'>"+deys+"</div><div class='hours'>"+hours+"</div><div class='minutes'>"+minutes+"</div><div class=seconds>"+seconds+"</div></div>";
  // вывод времени
  el_timer = document.getElementById("timer");
  if (el_timer) el_timer.innerHTML = limit_str;
}
  //Запуск таймера
    processTimer();
*/
/*--------------------------------------------------*/

//Таймер
var today = new Date();
var end = new Date(today.getFullYear(),today.getMonth(),today.getDate()+3, 00, 00, 00);
var action_day = end.getDate();
var action_month = end.getMonth();
switch(action_month){
    case 0:
        action_month = 'января';
        break;
    case 1:
        action_month = 'февраля';
        break;
    case 2:
        action_month = 'марта';
        break;
    case 3:
        action_month = 'апреля';
        break;
    case 4:
        action_month = 'мая';
        break;
    case 5:
        action_month = 'июня';
        break;
    case 6:
        action_month = 'июля';
        break;
    case 7:
        action_month = 'августа';
        break;
    case 8:
        action_month = 'сентября';
        break;
    case 9:
        action_month = 'октября';
        break;
    case 10:
        action_month = 'ноября';
        break;
    case 11:
        action_month = 'декабря';
        break;
}
function CountBox() {

    var now = new Date();
    var amount = end.getTime() - now.getTime();

    amount = Math.floor(amount / 1e3);
    var days = Math.floor(amount / 86400);
    var days1 = (days >= 10) ? days.toString().charAt(0) : '0';
    var days2 = (days >= 10) ? days.toString().charAt(1) : days.toString().charAt(0);
    amount = amount % 86400;
    var hours = Math.floor(amount / 3600);
    var hours1 = (hours >= 10) ? hours.toString().charAt(0) : '0';
    var hours2 = (hours >= 10) ? hours.toString().charAt(1) : hours.toString().charAt(0);
    amount = amount % 3600;
    var mins = Math.floor(amount / 60);
    var mins1 = (mins >= 10) ? mins.toString().charAt(0) : '0';
    var mins2 = (mins >= 10) ? mins.toString().charAt(1) : mins.toString().charAt(0);
    amount = amount % 60;
    var secs = Math.floor(amount);
    var secs1 = (secs >= 10) ? secs.toString().charAt(0) : '0';
    var secs2 = (secs >= 10) ? secs.toString().charAt(1) : secs.toString().charAt(0);
    var out = '<div class="timer_block"><div class="deys">'+days1+days2+'</div><div class="hours">'+hours1+hours2+'</div><div class="minutes">'+mins1+mins2+'</div><div class="seconds">'+secs1+secs2+'</div></div>';
    el_timer = document.getElementById("timer");
    if (el_timer) el_timer.innerHTML = out;
    setTimeout("CountBox()", 1e3)
}

window.onload = function () {
    CountBox();
    document.getElementById('end-timer-date').innerHTML = action_day + ' ' + action_month;
}
