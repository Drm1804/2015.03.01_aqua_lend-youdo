<?php
$error = false;
$name = '';
$phone = '';
$question = '';
$utm_medium = '';
$utm_source = '';
$utm_campaign = '';
$utm_term = '';
$utm_content = '';
$cm_title = '';
$location = '';


//taking info about date

$timestamp = date("Y-m-d H:i:s");
//taking the data from form

if(!empty($_POST['name'])) {
    $name = htmlspecialchars(trim($_POST['name']), ENT_QUOTES);
    if(empty($name)){
        $error = true;
    }
}
if(!empty($_POST['phone'])) {
    $phone = htmlspecialchars(trim($_POST['phone']), ENT_QUOTES);
    if(empty($phone)){
        $error = true;
    }
} else {
    $error = true;
}
if(!empty($_POST['email'])) {
    $email = htmlspecialchars(trim($_POST['email']), ENT_QUOTES);
    if(empty($email)){
        $email = true;
    }
} else {
    $error = true;
}
if(!empty($_POST['question'])) {
    $question = htmlspecialchars(trim($_POST['question']), ENT_QUOTES);
    if(empty($question)){
        $error = true;
    }
}

$location = htmlspecialchars(trim($_POST['location']), ENT_QUOTES);

//preparing mail
if(!$error) {
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $headers .= "Content-Transfer-Encoding: quoted-printable\n";
    $headers .= "From: LP_Aqua_Aerobic\n";

    $content = "<html><body><table border='1' style='border-color: #666;' cellpadding='10'>" .
        "<tr style='background: #eee;'><td><strong>Телефон посетителя:</strong> </td><td>".$phone."</td></tr>\n\n";
    if($name != ''){
        $content .= "<tr><td><strong>Имя посетителя:</strong> </td><td>".$name."</td></tr>\n\n";
    }
    if($email != ''){
        $content .= "<tr><td><strong>Электронная почта</strong> </td><td>".$email."</td></tr>\n\n";
    }
    if($object != ''){
        $content .= "<tr><td><strong>Запись на прием:</strong> </td><td>".$object."</td></tr>\n\n";
    }


    mail('drm-a@yandex.ru, 2513546@mail.ru',$location, $content, $headers);

    //redirect to thank-you.html page.
    header('location:./thanks.html');
} else {
    echo '<h1>Error!</h1>';
}
?>